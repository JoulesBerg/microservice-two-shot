from django.contrib import admin
from .models import Bin, Location

# Register your models here.
admin.site.register(Location)
admin.site.register(Bin)