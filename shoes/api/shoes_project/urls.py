"""shoes_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from shoes_rest.api_views import shoe_list, shoe_detail,bin_list, manufacturer_list, model_list

urlpatterns = [
    path('admin/', admin.site.urls),
    path('shoes/api/', shoe_list, name="shoe_list"),
    path('shoes/api/<int:pk>/', shoe_detail, name="shoe_detail"),
    path('bins/', bin_list, name="bin_list"),
    path('manufacturers/', manufacturer_list, name="manufacturer_list"),
    path('models/', model_list, name="model_list"),
]
