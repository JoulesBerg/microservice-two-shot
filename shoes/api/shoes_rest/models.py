from django.db import models
from django.contrib import admin
from django.urls import reverse

# Create your models here.

class Shoe(models.Model):
    name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200)
    picture_href = models.URLField(max_length=200)
    bin = models.ForeignKey("BinVO", related_name="shoes", on_delete=models.PROTECT)
    manufacturer = models.ForeignKey("Manufacturer", related_name="shoes", on_delete=models.PROTECT)
    model = models.ForeignKey("Model", related_name="shoes", on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("shoe_detail", kwargs={"pk": self.pk})

class BinVO(models.Model):
    name = models.CharField(max_length=250, null=True)
    href = models.URLField(max_length=250)

    def __str__(self):
        return self.name

class Model(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Manufacturer(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

