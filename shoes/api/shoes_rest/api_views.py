from django.views.decorators.http import require_http_methods
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from .models import Shoe, BinVO, Model, Manufacturer
from common.json import ModelEncoder
import json


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
    ]
    def get_extra_data(self, o):
        return {"href": o.get_api_url()}

class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = ["name"]


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "href",
    ]

class ShoeModelEncoder(ModelEncoder):
    model = Model
    properties = ["name"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "name",
        "color",
        "picture_href",
        "bin",
        "manufacturer",
        "model",
    ]

    encoders = {
        "bin": BinVOEncoder(),
        "manufacturer": ManufacturerEncoder(),
        "model": ShoeModelEncoder(),
    }


@require_http_methods(["POST", "GET"])
def shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            #Converts JSON request.body.bin object to an instance of bin -> by obtaining it from the database with its unique identifier (href)
            content["bin"] = json.loads(content["bin"])
            bin = BinVO.objects.get(href=content["bin"]["href"])
            content["bin"] = bin

            #Converts request.body.manufacturer into an instance and stores it in content 
            content["manufacturer"] = Manufacturer.objects.get(name=content['manufacturer'])

            #Converts request.body.model into an instance and stores it 
            content["model"] = Model.objects.get(name=content['model'])

            shoe = Shoe.objects.create(**content)

            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )

        except BinVO.DoesNotExist or Manufacturer.DoesNotExist or Model.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe href"},
                status=400
            )


@require_http_methods(["DELETE", "PUT", "GET"])
def shoe_detail(request, pk):

    if request.method == "PUT":
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.filter(id=pk).update(**content)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "This shoe not exist"})
    elif request.method =="DELETE":
        try:
            shoe = Shoe.objects.filter(id=pk).delete()
            return JsonResponse(
                shoe,
                ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "This shoe not exist"})
    else: #GET
        try:
            shoe = Shoe.objects.filter(id=pk)
            JsonResponse(
                shoe,
                ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "This shoe not exist"})


@require_http_methods(["GET"])
def manufacturer_list(request):
    if request.method == "GET":
        manufacturers = Manufacturer.objects.all()
    return JsonResponse({"manufacturers": manufacturers}, encoder=ManufacturerEncoder, safe=False)

@require_http_methods(["GET"])
def model_list(request):
    if request.method == "GET":
        models = Model.objects.all()
    return JsonResponse({"models":models}, encoder=ShoeModelEncoder, safe=False)

@require_http_methods(["GET"])
def bin_list(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
    return JsonResponse({"bins":bins}, encoder=BinVOEncoder, safe=False)

# @require_http_methods(["GET"])
# def manufacturers_bins_models_lists(request):
#     if request.method == "GET":
#         bins = BinVO.objects.all()
#         manufacturers = Manufacturer.objects.all()
#         models = Model.objects.all()
#     return JsonResponse({
#         "bins": bins,
    #     "models": models,
    #     "manufacturers": manufacturers
    #     },
    #     encoders={"bins": BinVOEncoder, "manufacturers": ManufacturerEncoder, "models": ShoeModelEncoder},
    #     safe=False
    # )
        # return json.dumps(json_response())

# def json_response():
#     bins = BinVO.objects.all()
#     manufacturers = Manufacturer.objects.all()
#     models = Model.objects.all()

#     response = {}
#     response["bins"] = JsonResponse({"bins": bins}, encoder=BinVOEncoder, safe=False)
#     response["models"] = JsonResponse({"models": models}, encoder=ShoeModelEncoder, safe=False)
#     response["manufacturers"] = JsonResponse({"manufacturers": manufacturers}, encoder=ManufacturerEncoder, safe=False)

#     return response
