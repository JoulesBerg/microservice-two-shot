import { BrowserRouter, Routes, Route, Outlet } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeDetail from './ShoeDetail';
import ShoeForm from './ShoeForm';





//import HatsDetail from './HatsDetail'
import HatsList from './HatsList'
//import HatsForm from './HatsForm' //I cannot figure out the hook error that is occuring for the life of me. I am not breaking any hook rules as far as I can tell, and I am extremely confused.

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoeList />} />
            <Route path="api/:id" element={<ShoeDetail />} />
            <Route path="form" element= {<ShoeForm />} />
          </Route>
          <Route path = "hats/" element={<HatsList/>}/>
            {/* <Route path = ":id" element = {<HatsDetail/>}/> */}
          {/* <Route path = "hats/new" element = {<HatsForm/>}/> */} 
            
        </Routes>
        {/* <Outlet/> */}
      </div>
      
    </BrowserRouter>
    
  );
}

export default App;
