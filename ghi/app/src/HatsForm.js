import React, { useState, useEffect, } from 'react';

function HatsForm(){
    const [options, updateOptions] = useState()
    console.log("first hook")
    async function fetchOptions(){
        const resp = await fetch("http://localhost:8090/hats/options")
        const optionsJson = await resp.json()
        updateOptions(optionsJson.options)
     }
    useEffect(()=>{
         return function cleanup(){
            fetchOptions()
         }
    },[])
    console.log("second hook")
    const[form, setForm] = useState({
        location: '',
        name: '',
        picture_href: '',
        fabric: '',
        style: '',
    })
    console.log('third hook')
    // const fetchOptions = {
    //     method : "POST",
    //     body: JSON.stringify(form),
    // }
    const handleNameInputChange = (event) => {
        event.persist()
        setForm((form) => ({
            ...form,
            name: event.target.value,
        }))
    }
    const handleStyleInputChange = (event) => {
        event.persist()
        setForm((form) => ({
            ...form,
            style: event.target.value,
        }))
    }
    const handleFabricInputChange = (event) => {
        event.persist()
        setForm((form) => ({
            ...form,
            fabric: event.target.value,
        }))
    }
    const handlePictureInputChange = (event) => {
        event.persist()
        setForm((form) => ({
            ...form,
            picture_href: event.target.value,
        }))
    }
    const handleLocationInputChange = (event) => {
        event.persist()
        setForm((form) => ({
            ...form,
            location: event.target.value,
        }))
    }
    

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange = {handleNameInputChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange = {handlePictureInputChange} placeholder="Picture Link" required type="text" name="picture_href" id="picture_ref" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="mb-3">
                  <select onChange = {handleStyleInputChange} required name="style" id="style" className="form-select">
                    <option value="">Choose a location</option>
                    {options.styles.map(style => {
                      return (
                        <option key={style.id} value={style.id}>{style.name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange = {handleFabricInputChange} required name="fabric" id="fabric" className="form-select">
                    <option value="">Choose a fabric</option>
                    {options.fabrics.map(fabric => {
                      return (
                        <option key={fabric.id} value={fabric.id}>{fabric.name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange = {handleLocationInputChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {options.locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>{location.name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    )
}


export default HatsForm()