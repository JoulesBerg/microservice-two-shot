import React from 'react'

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ...this.props,
            bins: [],
            manufacturers: [],
            models: [],

        }
    }

    async componentDidMount() {
        const binsResponse = await fetch("http://localhost:8080/bins/");
        const manufacturersReponse = await fetch("http://localhost:8080/manufacturers/");
        const modelsResponse = await fetch("http://localhost:8080/models/");

        if (binsResponse.ok && manufacturersReponse.ok && modelsResponse) {
            const modelsData = await modelsResponse.json();
            const manufacturersData = await manufacturersReponse.json();
            const binsData = await binsResponse.json();

            this.setState({
                "bins": binsData.bins,
                "manufacturers": manufacturersData.manufacturers,
                "models": modelsData.models
            })
        }
    }

    handleInput = e => {
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    handleFormSubmission = async e => {
        e.preventDefault();
        const data = {...this.state};
        delete data.bins;
        delete data.models;
        delete data.manufacturers;

        const url = "http://localhost:8080/shoes/api/"
        const fetchConfig = {
            method: "POST",
            header: {'Content-Type': 'application/json'},
            body: JSON.stringify(data),
        }
        
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            this.setState = ({
                name: '',
                color: '',
                picture_href: '',
                bin: '',
                manufacturer: '',
                model: '',
                // bins: null,
                // manufacturers: null,
                // models: null,
            });
        }

    }

    render() {
        return (
            <div className='container m-5'>
                <form onSubmit={this.handleFormSubmission}>
                    <div className="form-group m-3">
                        <label htmlFor="name">Name</label>
                        <input onChange={this.handleInput} name="name" type="text" className="form-control" id="name" aria-describedby="nameHelp" placeholder="Shoe Name"/>
                    </div>
                    <div className="form-group m-3">
                        <label htmlFor="color">Color</label>
                        <input onChange={this.handleInput} name="color" type="text" className="form-control" id="color" placeholder="Shoe Color"/>
                    </div>
                    <div className="form-group m-3">
                        <label htmlFor="picture_href">Picture</label>
                        <input onChange={this.handleInput} name="picture_href" type="url" className="form-control" id="picture_href" placeholder="Picture of Shoe"/>
                    </div>
                    <div className="form-group m-3">
                        <label htmlFor="bin">Bin</label>
                        <select onChange={this.handleInput} name="bin" id="bins" className="form-control text-muted">
                            <option value="bin">Bins</option>
                            {this.state.bins.map( bin => {
                                return (
                                <option key={bin.href} value={JSON.stringify(bin)}>{bin.name}</option>
                                )
                            })}
                            
                        </select>
                    </div>
                    <div className="form-group m-3">
                        <label htmlFor="manufacturer">Manufacturer</label>
                        <select onChange={this.handleInput} name="manufacturer" id="manufacturer" className="form-control text-muted">
                            <option value="manufacturer">Manufacturer</option>
                            {this.state.manufacturers.map( manufacturer => {
                                return (
                                <option key={manufacturer.name} value={manufacturer.name}>{manufacturer.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="form-group m-3">
                        <label htmlFor="model">Model</label>
                        <select onChange={this.handleInput} name="model" id="model" className="form-control text-muted">
                            <option value="model">Model</option>
                            {this.state.models.map( model => {
                                return (
                                <option key={model.name} value={model.name}>{model.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary m-3">Submit</button>
                </form>
            </div>
        )
    }

}

export default ShoeForm