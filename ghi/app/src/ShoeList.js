import React from 'react'
import {Link, NavLink} from 'react-router-dom'
import ShoeDetail from './ShoeDetail';

class ShoeList extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {
            shoes:[],
        }
    }

    async componentDidMount() {
        const response = await fetch("http://localhost:8080/shoes/api/");

        if (response.ok) {
            const data = await response.json();
            this.setState({
                ...this.state,
                ...data
            })
        }
    }

    delete = async (event, shoe) => {
        const response = await fetch(
            `http://localhost:8080${shoe.href}`,
            {method: "DELETE"}
        )
        console.log(response)

        //Page not rerendering?
        window.location.reload()
    }

    render() {
        return(
            <div className='container m-5'>
                <div className="list-group">
                    {this.state.shoes.map(shoe => {
                        return (
                            <div className='d-flex m-1' key={shoe.href}>
                                <div className="container-fluid">
                                    <NavLink to={shoe.href} state={{"props": shoe}} className="list-group-item list-group-item-action">
                                        {shoe.name}
                                    </NavLink>
                                </div>
                                <button onClick={ e => this.delete(e, shoe)} type="button" className="btn btn-danger">Delete</button>
                            </div>
                            
            
                            // <NavLink to={{
                            //     pathname: shoe.href 
                            // }}
                            //     state= {{shoe:'hi'}}
                            //     className="list-group-item list-group-item-action">
                            //     {shoe.name}
                            //     {console.log(shoe.href)}
                            // </NavLink>
                        )
                    })}
                </div>
                <button type="button" className="btn">
                    <NavLink to="form" className="list-group-item list-group-item-action">
                        Create New Shoe
                    </NavLink>
                </button>
            </div>
        )
    }
}

export default ShoeList