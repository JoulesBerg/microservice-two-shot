import React, { useState,useEffect,useCallback } from 'react';
//import {useNavigate} from 'react-router-dom';

function HatsList(){
    //const navigate = useNavigate()
    const [hats, updateHats]= useState()
    async function fetchData(){
        let resp = await fetch("http://localhost:8090/api/hats")
        let hatsJson = await resp.json()
        updateHats(hatsJson.hats)
        }
        
    useEffect(()=>{
        return function cleanup(){
            fetchData()
        }
    },[])
    const deleteHat = useCallback(async(id)=>{
        const url = `http://localhost:8090/api/hats/${id}`
        const fetchOptions = {
            method:"DELETE"
        }
        try{
        const resp = await fetch(url,fetchOptions)
        // const deleted = resp.json()
        if (resp.ok){
            fetchData()
        }
        }
        catch(e){
            console.error(e)
        }

    },[])

    if (hats === undefined) {
        return 'Loading...';
    }

    return(
            
        <div className = "card-holder">
            {hats.map(hat=>{
                return(
                    
                    <div className="card">
                        <img src={hat.picture_href} className="card-img-top"/>
                        <div className="card-body">
                        <h5 className="card-title">{hat.name}</h5>
                        <p className= "card-text">Style: {hat.style.name}</p>
                        <p className= "card-text">Fabric: {hat.fabric.name}</p>
                        <p className= "card-text">Location: {hat.location.name}</p>
                        </div>
                        <div className="card-footer">
                            <button
                                onClick={()=>{
                                    deleteHat(hat.id)
                                }}
                            >
                                Delete Hat
                            </button>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}


export default HatsList