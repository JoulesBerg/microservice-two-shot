from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Hat, LocationVO, Fabric, Accessories, Style
import json

from common.json import ModelEncoder

# Create your views here.

class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "href",
    ]
    def get_extra_data(self, o):
        return {
            "id": o.id
        }


class FabricEncoder(ModelEncoder):
    model = Fabric
    properties = [
        "name",
        "description",
    ]
    def get_extra_data(self, o):
        return {
            "id": o.id
        }


class StyleEncoder(ModelEncoder):
    model = Style
    properties = [
        "name",
        "description",
    ]
    def get_extra_data(self, o):
        return {
            "id": o.id
        }

class AccessoriesEncoder(ModelEncoder):
    model = Accessories
    properties = [
        "name",
        "description"
    ]
    def get_extra_data(self, o):
        return {
            "id": o.id
        }

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "location",
        "fabric",
        "style",
        "picture_href",
        # "accessories",
    ]

    encoders = {
        "location": LocationEncoder(),
        "fabric": FabricEncoder(),
        "style": StyleEncoder(),
        # "accessories": AccessoriesEncoder(),
    }
    def get_extra_data(self, o):
        return {
            "href" : o.get_api_url(),
            "id": o.id
        }

@require_http_methods(["GET","POST"])
def api_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:
        content = json.loads(request.body)
        # photo = get_photo(content["city"], content["state"].abbreviation) Pexels potentially?
        # content.update(photo)
        try:
            location_id = int(content['location'])
            content['location'] = LocationVO.objects.get(id=location_id)
        except:
            print('An error occured while attempting to parse location id into location object')
        try:
            fabric_id = int(content['fabric'])
            content['fabric'] = Fabric.objects.get(id=fabric_id)
        except:
            print('An error occured while attempting to parse fabric id into fabric object')
        try:
            style_id = int(content['style'])
            content['style'] = Style.objects.get(id=style_id)
        except:
            print('An error occured while attempting to parse style id into style object')
        #I chose not to get accessories up and running yet due to some bugs that were taking a lot of time to try to resolve and I did not have access to help over the weekend.
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["PUT","DELETE"])
def api_hat(request, pk):
    if request.method =="PUT":
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {"hat": hat},
            encoder=HatEncoder,
            safe=False
        )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET"])
def api_form_options(request):
    locations_objects = LocationVO.objects.all()
    fabrics_objects = Fabric.objects.all()
    styles_objects = Style.objects.all()
    locations = []
    fabrics = []
    styles = []
    for location in locations_objects:
        loc = {"id":location.id,"name":location.name}
        locations.append(loc)
    for fabric in fabrics_objects:
        fab = {"id":fabric.id,"name":fabric.name}
        fabrics.append(fab)
    for style in styles_objects:
        sty = {"id":style.id,"name":style.name}
        styles.append(sty)
    return JsonResponse({
        "options": {"styles":styles,"fabrics":fabrics,"locations":locations}
    })





# @require_http_methods(["POST"])  a different solution that ended up not being needed
# def update_locations(request):
#     content = json.loads(request.body)
#     for location in content["locations"]:
#         LocationVO.objects.update_or_create(
#             href=location["href"],
#             defaults={"name": location["name"]},
#         )

