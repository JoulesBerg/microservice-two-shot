from django.urls import path
from .views import api_hats, api_hat, api_form_options #update_locations


urlpatterns = [
    path("hats/", api_hats, name="api_hats"),
    path("hats/<int:pk>/", api_hat, name="api_hat"),
    path("hats/options/",api_form_options,name="api_form_options")
    # path("hats/updatelocations",update_locations,name="update_locations")
]