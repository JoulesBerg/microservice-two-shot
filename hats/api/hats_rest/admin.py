from django.contrib import admin

# Register your models here.
from .models import Hat, Accessories, Fabric, Style


@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    pass

@admin.register(Accessories)
class AccessoriesAdmin(admin.ModelAdmin):
    pass

@admin.register(Fabric)
class FabricAdmin(admin.ModelAdmin):
    pass

@admin.register(Style)
class StyleAdmin(admin.ModelAdmin):
    pass
