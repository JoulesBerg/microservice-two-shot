from django.db import models
from django.urls import reverse
# Create your models here.

class LocationVO(models.Model):
    href = models.URLField()
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name

class Fabric(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=300)
    def __str__(self):
        return self.name

class Style(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=300)
    def __str__(self):
        return self.name

class Accessories(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    def __str__(self):
        return self.name

class Hat(models.Model):
    name = models.CharField(max_length=100)
    picture_href = models.URLField(null=True)
    location = models.ForeignKey(LocationVO,related_name='hats',on_delete=models.PROTECT)
    fabric = models.ForeignKey(Fabric, related_name='hats',on_delete=models.PROTECT)
    accessories = models.ManyToManyField(Accessories,related_name='hats',blank=True)
    style = models.ForeignKey(Style,related_name='hats',on_delete=models.PROTECT)

    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})


    def __str__(self):
        return f"{self.fabric} {self.style}"