import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import LocationVO

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            #polling logic
            response = requests.get("http://wardrobe-api:8000/api/locations/")
            content = json.loads(response.content)
            for location in content["locations"]:
                LocationVO.objects.update_or_create(
                href=location["href"],
                defaults={
                    "name": location["closet_name"]+" section "+str(location['section_number'])+" shelf "+str(location['shelf_number']),
                    },
                )
            # requests.post(url="http://localhost:8000/api/hats/updatelocations",data = response)
            # response = requests.get("wardrobe-api:8000")
            # requests.post("hats-api:8000",data = response)
        except Exception as e:
            print('error')
            print(e)
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
